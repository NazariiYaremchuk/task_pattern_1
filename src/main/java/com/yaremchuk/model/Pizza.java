package com.yaremchuk.model;

public interface Pizza {
    void prepare();

    void bake();

    void cut();

    void box();
}
