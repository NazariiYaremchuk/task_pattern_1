package com.yaremchuk.model;

public enum PizzaType {
    CHEESE, VEGGIE, CLAM, PEPPERONI
}

