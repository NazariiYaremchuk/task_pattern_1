package com.yaremchuk.model.pizzaImp;

import com.yaremchuk.model.Pizza;

public class ClamPizza implements Pizza {
    @Override
    public void prepare() {
    }

    @Override
    public void bake() {
    }

    @Override
    public void cut() {
    }

    @Override
    public void box() {
    }
}
